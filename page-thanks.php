<?php
/*
Template Name: Thanks
*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Labs - Games Store</title>
  <meta charset="UTF-8">
  <meta name="description" content="Labs - Games Store">
  <meta name="keywords" content="games, steam, sale">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <?php wp_head(); ?>
</head>
<body>
<!-- Page Preloder -->
<div id="preloder">
  <div class="loader">
    <img src="<?php echo bloginfo('template_url'); ?>/assets/img/logo.png" alt="">
    <h2>Loading.....</h2>
  </div>
</div>

	<!-- Services section -->
<div class="services-section spad" id="services">
	<div class="container">
		<div class="section-title dark">
			<h2>Thanks for your order! We got it!</h2>
		</div>
	</div>
</div>
<!-- services section end -->

<?php
get_footer();
?>