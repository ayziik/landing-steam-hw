<form action="#"
      method="post" class="form-class" id="con_form">
  <div class="row">
    <div class="col-sm-12">
      <h3>Choose from the most popular:</h3>

        <?php
        $args = array(
            'post_type'      => 'product',
            'posts_per_page' => 5,
            'product_cat'    => 'white'
        );

        $loop = new WP_Query( $args );

        while ( $loop->have_posts() ) : $loop->the_post();
            global $product;
            $price = get_post_meta( get_the_ID(), '_price', true );
            $stock = get_post_meta( get_the_ID(), '_stock', true );

            echo '<label class="containerr">'
                 . get_the_title() . ' - Price: '
                 . wc_price( $price ) . ' - In stock: '
                 . $stock . ' '
                 . '<input type="checkbox" name="' . get_the_title() . '">'
             . '<span class="checkmarkr"></span>
              </label>';
        endwhile;

        wp_reset_query();
        ?>

      <h3>Or find what you need:</h3>
      <input class="dataList" name="datalist" list="character" placeholder="Other games">
      <datalist id="character">
          <?php
          $args = array(
              'post_type'      => 'product',
              'posts_per_page' => -1,
              'product_cat'    => 'white'
          );

          $loop = new WP_Query( $args );

          while ( $loop->have_posts() ) : $loop->the_post();
              global $product;
              $price = get_post_meta( get_the_ID(), '_price', true );
              $stock = get_post_meta( get_the_ID(), '_stock', true );

              echo '<option name="game" value="' . get_the_title() . '">'
                   . get_the_title() . ' - Price: '
                   . wc_price( $price ) . ' - In stock: '
                   . $stock . ' '
                   . '</option>';
          endwhile;

          wp_reset_query();
          ?>
      </datalist>
    </div>

    <div class="col-sm-6">
      <input type="text" name="name" placeholder="Your name" required>
    </div>
    <div class="col-sm-6">
      <input type="text" name="email" placeholder="Your email" required>
    </div>

    <input type="hidden" name="category" value="white">

    <div class="col-sm-12">
      <button class="site-btn">Order</button>
    </div>
  </div>
</form>