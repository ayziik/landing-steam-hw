<?php
/*
Template Name: Thanks
*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <!-- Title -->
  <title>Egames - Gaming Magazine</title>

  <!-- Favicon -->
  <link rel="icon" href="<?php echo bloginfo('template_url'); ?>/assets/img/core-img/favicon.ico">

  <?php wp_head(); ?>
</head>
<body class="thanks-page-body">
<!-- ##### Breadcrumb Area Start ##### -->
<section class="breadcrumb-area bg-img bg-overlay thanks-page" style="background-image: url(img/bg-img/23.jpg);">
  <div class="container h-100">
    <div class="row h-100 align-items-center">
      <!-- Breadcrumb Text -->
      <div class="col-12">
        <div class="breadcrumb-text">
          <h2>Thanks for your order! We got it!</h2>
        </div>

        <div class="wrapper-about">
          <div class="col-12 col-md-4">
            <div class="single-games-area text-center mb-100 wow fadeInUp" data-wow-delay="500ms">
              <a href="<?php echo get_home_url() ?>" class="btn egames-btn mt-30">Go to Home Page</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

</body>

</html>