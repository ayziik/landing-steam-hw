<?php
$args = array(
    'post_type'      => 'product',
    'posts_per_page' => -1,
    'product_cat'    => 'black'
);

$loop = new WP_Query( $args );
?>

<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

    <?php
    global $product;
    $price = get_post_meta( get_the_ID(), '_price', true );
    $stock = get_post_meta( get_the_ID(), '_stock', true );
    ?>

    <?php  if ($stock > 0) : ?>
        <div class="single-games-slide">
            <img src="<?php the_post_thumbnail_url(); ?>" alt="">
            <div class="slide-text">
                <a href="#buy-now" class="game-title"><?php echo get_the_title(); ?></a>
                <div class="meta-data">
                    <a href="#buy-now">Price: $<?php echo $price; ?></a>
                    <a href="#buy-now">BUY NOW</a>
                </div>
            </div>
        </div>
    <?php  endif;  ?>
<?php endwhile; ?>
<?php wp_reset_query(); ?>
