<?php

function gmailList() {
	global $wpdb;

	// Проверка на Gmail, чтобы добавить в лист рассылки
	$pattern = '/^[A-Za-z0-9][A-Za-z0-9\.\-_]*[A-Za-z0-9]*@gmail.com$/';
	$isGmail = preg_match($pattern, $_POST['email']);

	if ($isGmail) {
		$isUserInList = $user = $wpdb->get_row( $wpdb->prepare(
			"SELECT * FROM wp_ig_contacts WHERE email = %s", $_POST['email']
		) );

		if (!$isUserInList) {
			$wpdb->query(
				$wpdb->prepare(
					"INSERT INTO wp_ig_contacts ( first_name, email, status, is_verified) VALUES ( %s, %s, %s, %d )",
					$_POST['name'],
					$_POST['email'],
					'verified',
					1
				)
			);

			$user = $wpdb->get_row( $wpdb->prepare(
				"SELECT * FROM wp_ig_contacts WHERE email = %s", $_POST['email']
			) );
			$userId = $user->id;

			$list = $wpdb->get_row( $wpdb->prepare(
				"SELECT * FROM wp_ig_lists WHERE slug = %s", 'users-with-gmail'
			) );
			$listId = $list->id;

			$wpdb->query(
				$wpdb->prepare(
					"INSERT INTO wp_ig_lists_contacts ( list_id, contact_id, status, optin_type) VALUES ( %d, %d, %s, %d )",
					$listId,
					$userId,
					'subscribed',
					1
				)
			);
		}
	}
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$page     = get_page_by_path( 'thanks' );
	$pageLink = get_page_link( $page->ID );
	header( "Location: {$pageLink}" );

	foreach ($_POST as $key => $value) {
		if ($value === 'on') {
			$pattern = '/ID\d+$/';
			$findId = preg_match($pattern, $key, $matches);

			$id = str_replace('ID', '', $matches[0]);
			$goodsInOrder[] = $id;
		}

		if ($key === 'datalist') {
			$findId = preg_match($pattern, $value, $matches);

			$id = str_replace('ID', '', $matches[0]);
			$goodsInOrder[] = $id;
		}
	}

	$address = array(
		'first_name' => $_POST['name'],
		'email'      => $_POST['email'],
	);


	$order = wc_create_order();

	foreach ($goodsInOrder as $value) {
		$product = new WC_Product($value);
		$order->add_product( $product, 1 );
	}

	$order->set_address( $address, 'billing' );
	$order->calculate_totals();
	$order->update_status( 'wc-processing', 'Order created dynamically - ', TRUE);

	gmailList();
}