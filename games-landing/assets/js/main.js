const orderForm = document.getElementById('order-form');
const nameInput = document.getElementById('name');
const emailInput = document.getElementById('email');
const dataList = document.getElementById('datalist');
const checkboxes = document.querySelectorAll('.check');
const noGames = document.querySelector('.no-games');

function firstNameValidation(firstName) {
  let testFirstName = /^[A-Za-zА-ЯЁа-яё]{3,}$/g.test(firstName);
  let errorMessage = document.querySelector('.valid-firstname');

  if (!testFirstName) {
    errorMessage.classList.add('input--invalid');
    nameInput.classList.add('input-error');

    return false;
  } else {
    errorMessage.classList.remove('input--invalid');
    nameInput.classList.remove('input-error');

    return true;
  }
}

function emailValidation(email) {
  let testEmail = /^([A-Za-z0-9][A-Za-z0-9]*\.?\-?_?)*[A-Za-z0-9]*@([A-Za-z0-9]+([A-Za-z0-9-]*[A-Za-z0-9]+)*\.)+[A-Za-z]{2,}$/g.test(email);
  let errorMessage = document.querySelector('.valid-email');

  if (!testEmail) {
    errorMessage.classList.add('input--invalid');
    emailInput.classList.add('input-error');

    return false;
  } else {
    errorMessage.classList.remove('input--invalid');
    emailInput.classList.remove('input-error');

    return true;
  }
}

orderForm.addEventListener('submit', (e) => {
  e.preventDefault();

  let isCheckboxChecked = false;

  for (let i = 0; i < checkboxes.length; i++) {
      if (checkboxes[i].checked) {
        isCheckboxChecked = true;
      }
  }

  let isFirstNameValid = firstNameValidation(nameInput.value);

  let isEmailValid = emailValidation(emailInput.value);

  if (isFirstNameValid && isEmailValid ) {
    if (isCheckboxChecked) {
      orderForm.submit();
    } else {
      if (dataList.value) {
        noGames.classList.add('d-none');

        orderForm.submit();
      } else {
        noGames.classList.remove('d-none');
      }
    }
  }
})

