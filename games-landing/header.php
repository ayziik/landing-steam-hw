<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <!-- Title -->
  <title>Egames - Gaming Magazine</title>

  <!-- Favicon -->
  <link rel="icon" href="<?php echo bloginfo('template_url'); ?>/assets/img/core-img/favicon.ico">

  <?php wp_head(); ?>
</head>

<body>
<!-- Preloader -->
<div class="preloader d-flex align-items-center justify-content-center">
  <div class="spinner">
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
  </div>
</div>

<!-- ##### Header Area Start ##### -->
<header class="header-area wow fadeInDown" data-wow-delay="500ms">
  <!-- Navbar Area -->
  <div class="egames-main-menu" id="sticker">
    <div class="classy-nav-container breakpoint-off">
      <div class="container">
        <!-- Menu -->
        <nav class="classy-navbar justify-content-between" id="egamesNav">

          <!-- Navbar Toggler -->
          <div class="classy-navbar-toggler">
            <span class="navbarToggler"><span></span><span></span><span></span></span>
          </div>

          <!-- Menu -->
          <div class="classy-menu">

            <!-- Close Button -->
            <div class="classycloseIcon">
              <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
            </div>

            <!-- Nav Start -->
            <div class="classynav">

              <div class="nav-wrapper">
                <div class="logo">
                  <a href="#"><img src="<?php echo bloginfo('template_url'); ?>/assets/img/core-img/logo.png" alt=""></a>
                </div>

                <ul class="header-nav">
                  <li><a href="#about-us">About Us</a></li>
                  <li><a href="#games">Games</a>
                  </li>
                </ul>
              </div>
            <!-- Nav End -->
            </div>
          </div>
          <a href="#buy-now" class="buy-now egames-btn btn">BUY NOW</a>
        </nav>
      </div>
    </div>
  </div>
</header>
<!-- ##### Header Area End ##### -->