<?php

add_theme_support( 'woocommerce' );
add_action( 'wp_enqueue_scripts', 'hwAddScripts' );

function hwAddScripts() {
	wp_enqueue_style( 'hwMainStyle', get_stylesheet_uri() );
	wp_register_style( 'googleOswald', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800', array(), null, 'all' );
	wp_enqueue_style( 'googleOswald' );
	wp_enqueue_style( 'owlCarouselMin', get_template_directory_uri() . '/assets/css/owl.carousel.min.css' );
	wp_enqueue_style( 'fontAwesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css' );
	wp_enqueue_style( 'classy', get_template_directory_uri() . '/assets/css/classy-nav.css' );
	wp_enqueue_style( 'magnificPopup', get_template_directory_uri() . '/assets/css/magnific-popup.css' );
	wp_enqueue_style( 'bootstrapMin', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
    wp_enqueue_style( 'animate', get_template_directory_uri() . '/assets/css/animate.css' );
    wp_enqueue_style( 'niceSelect', get_template_directory_uri() . '/assets/css/nice-select.css' );


	wp_enqueue_script( 'bootstrapMinJs', get_template_directory_uri() . '/assets/js/bootstrap/bootstrap.min.js',
		array(), null, true );
    wp_enqueue_script( 'popper', get_template_directory_uri() . '/assets/js/bootstrap/popper.min.js',
        array(), null, true );
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery/jquery-2.2.4.min.js',
        array(), null, true );
    wp_enqueue_script( 'plugins', get_template_directory_uri() . '/assets/js/plugins/plugins.js',
        array(), null, true );
    wp_enqueue_script( 'active', get_template_directory_uri() . '/assets/js/active.js',
        array(), null, true );
    wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js',
        array(), null, true );
}