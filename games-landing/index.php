<?php
require_once( 'createOrder.php' );
?>

<?php
get_header();
?>

<!-- ##### Hero Area Start ##### -->
<div class="hero-area">
  <!-- Hero Post Slides -->
  <div class="hero-post-slides owl-carousel">

    <!-- Single Slide -->
    <div class="single-slide slideOne bg-img bg-overlay">
      <!-- Blog Content -->
      <div class="container h-100">
        <div class="row h-100 align-items-center">
          <div class="col-12 col-lg-9">
            <div class="blog-content" data-animation="fadeInUp" data-delay="100ms">
                <?php if ($_GET['fbclid']) : ?>
                  <h2 data-animation="fadeInUp" data-delay="400ms">Buy black Steam keys now with BIG DISCOUNT!</h2>
                <?php else : ?>
                  <h2 data-animation="fadeInUp" data-delay="400ms">Buy legal Steam keys now!</h2>
                <?php endif; ?>
              <p data-animation="fadeInUp" data-delay="700ms">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc tristique justo id elit bibendum pharetra non vitae lectus. Mauris libero felis, dapibus a ultrices sed, commodo vitae odio. Sed auctor tellus quis arcu tempus, egestas tincidunt.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Single Slide -->
    <div class="single-slide slideTwo bg-img bg-overlay">
      <!-- Blog Content -->
      <div class="container h-100">
        <div class="row h-100 align-items-center">
          <div class="col-12 col-lg-9">
            <div class="blog-content" data-animation="fadeInUp" data-delay="100ms">
                <?php if ($_GET['fbclid']) : ?>
                  <h2 data-animation="fadeInUp" data-delay="400ms">Buy black Steam keys now with BIG DISCOUNT!</h2>
                <?php else : ?>
                  <h2 data-animation="fadeInUp" data-delay="400ms">Buy legal Steam keys now!</h2>
                <?php endif; ?>
              <p data-animation="fadeInUp" data-delay="700ms">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc tristique justo id elit bibendum pharetra non vitae lectus. Mauris libero felis, dapibus a ultrices sed, commodo vitae odio. Sed auctor tellus quis arcu tempus, egestas tincidunt.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<!-- ##### Hero Area End ##### -->

<!-- ##### Games Area Start ##### -->
<div class="games-area section-padding-100-0" id="about-us">
  <div class="container">
    <div class="row">
      <!-- Single Games Area -->
      <div class="col-12 col-md-4">
        <div class="single-games-area text-center mb-100 wow fadeInUp" data-wow-delay="100ms">
          <h3>Lorem ipsum dolor</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur leo est, feugiat nec elementum id, suscipit id nulla..</p>
        </div>
      </div>

      <!-- Single Games Area -->
      <div class="col-12 col-md-4">
        <div class="single-games-area text-center mb-100 wow fadeInUp" data-wow-delay="300ms">
          <h3>Lorem ipsum dolor</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur leo est, feugiat nec elementum id, suscipit id nulla..</p>
        </div>
      </div>

      <!-- Single Games Area -->
      <div class="col-12 col-md-4">
        <div class="single-games-area text-center mb-100 wow fadeInUp" data-wow-delay="500ms">
          <h3>Lorem ipsum dolor</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur leo est, feugiat nec elementum id, suscipit id nulla..</p>
        </div>
      </div>


      <div class="wrapper-about">
        <div class="col-12 col-md-4">
          <div class="single-games-area text-center mb-100 wow fadeInUp" data-wow-delay="500ms">
            <a href="#buy-now" class="btn egames-btn mt-30">Buy Games</a>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- ##### Games Area End ##### -->

<!-- ##### Monthly Picks Area Start ##### -->
<section class="monthly-picks-area section-padding-100 bg-pattern" id="games">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="left-right-pattern"></div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-12">
        <!-- Title -->
        <h2 class="section-title mb-70 wow fadeInUp" data-wow-delay="100ms">This Month’s Pick</h2>
      </div>
    </div>

    <div class="row">
      <div class="col-12">
        <ul class="nav nav-tabs wow fadeInUp" data-wow-delay="300ms" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="popular-tab" data-toggle="tab" href="#popular" role="tab" aria-controls="popular" aria-selected="true">Games</a>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="tab-content wow fadeInUp" data-wow-delay="500ms" id="myTabContent">
    <div class="tab-pane fade show active" id="popular" role="tabpanel" aria-labelledby="popular-tab">
      <!-- Popular Games Slideshow -->
      <div class="popular-games-slideshow owl-carousel">
          <?php if ($_GET['fbclid']) : ?>
              <?php require_once 'blackSlider.php'?>
          <?php else : ?>
              <?php require_once 'whiteSlider.php'?>
          <?php endif; ?>
      </div>
    </div>
  </div>
</section>
<!-- ##### Monthly Picks Area End ##### -->

<!-- ##### Contact Area Start ##### -->
<section class="contact-area section-padding-100" id="buy-now">
  <div class="container">
    <div class="row">
      <!-- Contact Form Area -->
        <?php if ($_GET['fbclid']) : ?>
            <?php require_once 'blackForm.php'?>
        <?php else : ?>
            <?php require_once 'whiteForm.php'?>
        <?php endif; ?>
    </div>
  </div>
</section>
<!-- ##### Contact Area End ##### -->

<?php
get_footer();
?>
