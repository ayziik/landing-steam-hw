<div class="col-12">
  <h2 class="mb-30">Buy your favorite games!</h2>

  <!-- Contact Form -->
  <div class="contact-form-area mb-30">
    <form action="#" method="post" id="order-form">
      <div class="row">
        <div class="col-12">
          <h3>Choose from the most popular:</h3>

            <?php
            $args = array(
                'post_type'      => 'product',
                'posts_per_page' => 5,
                'product_cat'    => 'blackTop'
            );

            $loop = new WP_Query( $args );

            while ( $loop->have_posts() ) : $loop->the_post();
                global $product;
                $price = get_post_meta( get_the_ID(), '_price', true );
                $stock = get_post_meta( get_the_ID(), '_stock', true );

                if ($stock > 0) {
                    echo '<label class="containerr">'
                        . get_the_title() . ' - Price: '
                        . wc_price( $price ) . ' - In stock: '
                        . $stock . ' '
                        . '<input type="checkbox" class="check" name="' . get_the_title() . 'ID' . get_the_ID(). '">'
                        . '<span class="checkmarkr"></span>
              </label>';
                }
            endwhile;

            wp_reset_query();
            ?>

        </div>

        <div class="col-12">
          <h3>Or find what you need:</h3>
          <input class="dataList form-control  custom-input" id="datalist" name="datalist" list="character" placeholder="Other games">
          <datalist id="character">
              <?php
              $args = array(
                  'post_type'      => 'product',
                  'posts_per_page' => -1,
                  'product_cat'    => 'black'
              );

              $loop = new WP_Query( $args );

              while ( $loop->have_posts() ) : $loop->the_post();
                  global $product;
                  $price = get_post_meta( get_the_ID(), '_price', true );
                  $stock = get_post_meta( get_the_ID(), '_stock', true );

                  if ($stock > 0) {
                      echo '<option name="game" value="' . get_the_title() . ' - ID' . get_the_ID() . '">'
                          . get_the_title() . ' - Price: '
                          . wc_price( $price ) . ' - In stock: '
                          . $stock . ' '
                          . '</option>';
                  }
              endwhile;

              wp_reset_query();
              ?>
          </datalist>
        </div>

        <div class="col-12 col-lg-6">
          <input  name="name" type="text" class="form-control custom-input custom-input-mb" id="name" placeholder="Name*" required>
          <span class="valid-firstname">Name is invalid</span>
        </div>

        <div class="col-12 col-lg-6">
          <input  name="email" type="email" class="form-control  custom-input custom-input-mb" id="email" placeholder="Email*" required>
          <span class="valid-email">Email is invalid</span>
        </div>

        <p class="no-games d-none col-12">Choose at least one game</p>

        <div class="col-12">
          <button class="btn egames-btn w-100  order-btn" type="submit">ORDER</button>
        </div>
      </div>
    </form>
  </div>
</div>