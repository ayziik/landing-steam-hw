<!DOCTYPE html>
<html lang="en">
<head>
  <title>Labs - Games Store</title>
  <meta charset="UTF-8">
  <meta name="description" content="Labs - Games Store">
  <meta name="keywords" content="games, steam, sale">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="<?php echo bloginfo('template_url'); ?>/assets/img/favicon.ico" rel="shortcut icon"/>

	<?php wp_head(); ?>
</head>
<body>
<!-- Page Preloder -->
<div id="preloder">
  <div class="loader">
    <img src="<?php echo bloginfo('template_url'); ?>/assets/img/logo.png" alt="">
    <h2>Loading.....</h2>
  </div>
</div>


<!-- Header section -->
<header class="header-section">
  <div class="logo">
    <img src="<?php echo bloginfo('template_url'); ?>/assets/img/logo.png" alt=""><!-- Logo -->
  </div>
  <!-- Navigation -->
  <div class="responsive"><i class="fa fa-bars"></i></div>
  <nav>
    <ul class="menu-list">
      <li><a href="#reviews">Reviews</a></li>
      <li><a href="#services">Services</a></li>
      <li><a href="#buynow">Buy Now</a></li>
    </ul>
  </nav>
</header>
<!-- Header section end -->