<?php

function gmailList() {
	global $wpdb;

	// Проверка на Gmail, чтобы добавить в лист рассылки
	$pattern = '/^[A-Za-z0-9][A-Za-z0-9\.\-_]*[A-Za-z0-9]*@gmail.com$/';
	$isGmail = preg_match($pattern, $_POST['email']);

	if ($isGmail) {
		$isUserInList = $user = $wpdb->get_row( $wpdb->prepare(
			"SELECT * FROM wp_ig_contacts WHERE email = %s", $_POST['email']
		) );

		if (!$isUserInList) {
			$wpdb->query(
				$wpdb->prepare(
					"INSERT INTO wp_ig_contacts ( first_name, email, status, is_verified) VALUES ( %s, %s, %s, %d )",
					$_POST['name'],
					$_POST['email'],
					'verified',
					1
				)
			);

			$user = $wpdb->get_row( $wpdb->prepare(
				"SELECT * FROM wp_ig_contacts WHERE email = %s", $_POST['email']
			) );
			$userId = $user->id;

			$list = $wpdb->get_row( $wpdb->prepare(
				"SELECT * FROM wp_ig_lists WHERE slug = %s", 'users-with-gmail'
			) );
			$listId = $list->id;

			$wpdb->query(
				$wpdb->prepare(
					"INSERT INTO wp_ig_lists_contacts ( list_id, contact_id, status, optin_type) VALUES ( %d, %d, %s, %d )",
					$listId,
					$userId,
					'subscribed',
					1
				)
			);
		}
	}
}



if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$page     = get_page_by_path( 'thanks' );
	$pageLink = get_page_link( $page->ID );
	header( "Location: {$pageLink}" );

	// Подготавливаем имейл
	$to      = $_POST['email'];
	$subject = $_POST['name'] . ', thanks for your order! We got it!';
	$headers = [
		'From: labs@games.com',
		'content-type: text/html'
	];
	$message = 'You ordered: ' . '<br>';

	foreach ( $_POST as $key => $value ) {
		if ( $value === 'on' ) {
			$games[] = str_replace( '_', ' ', $key );
			$message .= '- ' . str_replace( '_', ' ', $key ) . '<br>';
		}
	}

	if ( isset( $_POST['datalist'] ) ) {
		$games[] = $_POST['datalist'];
		$message .= '- ' . $_POST['datalist'] . '<br>';
	}

	$message .= '<br>' . 'Our manager will contact you shortly.' . '<br>' . 'Have a nice day:)';

	//ОТправляем имейл
	$isEmailSendToCustomer = wp_mail( $to, $subject, $message, $headers );
	$emailStatus           = $isEmailSendToCustomer ? 'successfully sent' : 'Error, message not sent';

	// Добавляем заказ в базу
	global $wpdb;

	$wpdb->query(
		$wpdb->prepare(
			"INSERT INTO wp_games_landing ( name, email, games, email_send, category ) VALUES ( %s, %s, %s, %s, %s )",
			$_POST['name'],
			$_POST['email'],
			implode( ', ', $games ),
			$emailStatus,
			$_POST['category']
		)
	);

	gmailList();
}