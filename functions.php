<?php

add_theme_support( 'woocommerce' );
add_action( 'wp_enqueue_scripts', 'hwAddScripts' );

function hwAddScripts() {
	wp_enqueue_style( 'hwMainStyle', get_stylesheet_uri() );
	wp_register_style( 'googleOswald', 'https://fonts.googleapis.com/css?family=Oswald:300,400,500,700|Roboto:300,400,700', array(), null, 'all' );
	wp_enqueue_style( 'googleOswald' );
	wp_enqueue_style( 'owlCarouselMin', get_template_directory_uri() . '/assets/css/owl.carousel.css' );
	wp_enqueue_style( 'fontAwesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css' );
	wp_enqueue_style( 'flaticonStyle', get_template_directory_uri() . '/assets/css/flaticon.css' );
	wp_enqueue_style( 'magnificPopup', get_template_directory_uri() . '/assets/css/magnific-popup.css' );
	wp_enqueue_style( 'bootstrapMin', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );


	wp_enqueue_script( 'jqueryMin', get_template_directory_uri() . '/assets/js/jquery-2.1.4.min.js',
		array(), null, true );
	wp_enqueue_script( 'bootstrapMinJs', get_template_directory_uri() . '/assets/js/bootstrap.min.js',
		array(), null, true );
	wp_enqueue_script( 'owlCarouselMinJs', get_template_directory_uri() . '/assets/js/owl.carousel.min.js',
		array(), null, true );
	wp_enqueue_script( 'circle', get_template_directory_uri() . '/assets/js/circle-progress.min.js',
		array(), null, true );
	wp_enqueue_script( 'magnific', get_template_directory_uri() . '/assets/js/magnific-popup.min.js',
		array(), null, true );
	wp_enqueue_script( 'mainJs', get_template_directory_uri() . '/assets/js/map.js',
		array(), null, true );
	wp_enqueue_script( 'map', get_template_directory_uri() . '/assets/js/main.js',
		array(), null, true );
	wp_enqueue_script( 'respond', 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js',
		array(), null, true );
	wp_enqueue_script( 'html5shiv', 'https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js',
		array(), null, true );
}

// Создём свою таблицы для храниениия заказов

function create_table() {
	global $wpdb;

	require_once ABSPATH . 'wp-admin/includes/upgrade.php';

	$table_name = $wpdb->get_blog_prefix() . 'games_landing';
	$charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset} COLLATE {$wpdb->collate}";

	$sql = "CREATE TABLE {$table_name} (
	id int(11) unsigned NOT NULL auto_increment,
  name varchar(255) NOT NULL default '',
  email varchar(255) NOT NULL default '',
  games text NOT NULL default '',
  email_send varchar(255) NOT NULL default '',
  category varchar(255) NOT NULL default '',    
  created_at datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY  (id)
	)
	{$charset_collate};";

	dbDelta($sql);
}

create_table();

// Добавляем вывод заказов отдельным пуктом в меню

// сохранение опции экрана per_page. Нужно вызывать рано до события 'admin_menu'
add_filter( 'set-screen-option', function( $status, $option, $value ){
	return ( $option == 'lisense_table_per_page' ) ? (int) $value : $status;
}, 10, 3 );

// создаем страницу в меню, куда выводим таблицу
add_action( 'admin_menu', function(){
	$hook = add_menu_page( 'Orders', 'Orders', 'manage_options',
      'orders_page', 'example_table_page', 'dashicons-products', 50 );

	add_action( "load-$hook", 'example_table_page_load' );
} );

function example_table_page_load(){
	require_once __DIR__ . '/Orders.php'; // тут находится класс Orders

	// создаем экземпляр и сохраним его дальше выведем
	$GLOBALS['Orders'] = new Orders();
}

function example_table_page(){
	?>
	<div class="wrap">
		<h2><?php echo get_admin_page_title() ?></h2>

		<?php
		// выводим таблицу на экран где нужно
		echo '<form action="" method="POST">';
		$GLOBALS['Orders']->display();
		echo '</form>';
		?>

	</div>
	<?php
}

