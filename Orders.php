<?php

// расширять класс нужно после или во время admin_init
// класс удобнее поместить в отдельный файл.

class Orders extends WP_List_Table {

	function __construct(){
		parent::__construct(array(
			'singular' => 'log',
			'plural'   => 'logs',
			'ajax'     => false,
		));

		$this->prepare_items();

		add_action( 'wp_print_scripts', [ __CLASS__, '_list_table_css' ] );
	}

	// создает элементы таблицы
	function prepare_items(){
		global $wpdb;

	  // элементы таблицы
    $orders = $wpdb->get_results( "SELECT * FROM wp_games_landing ORDER BY id DESC" );

	  //Sets
	  $per_page = 50;

	  /* Устанавливаем данные для пагинации */
	  $this -> set_pagination_args( array(
		  'total_items' => count($orders),
		  'per_page'    => $per_page
	  ));

	  /* Делим массив на части для пагинации */
	  $data = array_slice(
	    $orders,
		  (($this -> get_pagenum() - 1) * $per_page),
		  $per_page
	  );

	  /* Устанавливаем данные таблицы */
	  $this->items = $data;
	}

	// колонки таблицы
	function get_columns(){
		return array(
		    'id'            => 'ID',
			  'name' => 'Name',
			  'email'   => 'Email',
        'games'            => 'Games',
        'category' => 'Category',
        'created_at'   => 'Date',
        'email_send' => 'Confirmation email'
		);
	}

	// вывод каждой ячейки таблицы -------------

	static function _list_table_css(){
		?>
		<style>
			table.logs .column-id{ width: 3%; }
			table.logs .column-name{ width: 10%; }
			table.logs .column-email{ width: 15%; }
      table.logs .column-games{ width: 25%; }
      table.logs .column-category{ width: 10%; }
      table.logs .column-created_at{ width: 10%; }
      table.logs .column-email_send{ width: 10%; }
		</style>
		<?php
	}

	// вывод каждой ячейки таблицы...
	function column_default( $item, $colname ){
			return isset($item->$colname) ? $item->$colname : 'Error';
	}
}
